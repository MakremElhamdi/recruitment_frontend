import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { AddUserComponent } from './components/add-user/add-user.component';
import { DeleteUserComponent } from './components/delete-user/delete-user.component';
import { ListUserComponent } from './components/list-user/list-user.component';
import {MatDialogActions, MatDialogClose, MatDialogContent} from "@angular/material/dialog";
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { DetailsUserComponent } from './components/details-user/details-user.component';
import { LoginUserComponent } from './components/login-user/login-user.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from "ngx-toastr";
import { UpdateUserComponent } from './components/update-user/update-user.component';
import { JobApplicationComponent } from './components/job-application/job-application.component';
import { JobOfferComponent } from './components/job-offer/job-offer.component';
import { ListJobOfferComponent } from './components/list-job-offer/list-job-offer.component';
import { ListJobApplierComponent } from './components/list-job-applier/list-job-applier.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { DeleteOfferComponent } from './components/delete-offer/delete-offer.component';
import { JobOfferDetailsComponent } from './components/job-offer-details/job-offer-details.component';
import {MatCard, MatCardActions, MatCardContent, MatCardHeader, MatCardModule} from "@angular/material/card";
import {MatButton} from "@angular/material/button";
import {MatPaginator} from "@angular/material/paginator";

@NgModule({
  declarations: [
    AppComponent,
    AddUserComponent,
    DeleteUserComponent,
    ListUserComponent,
    DetailsUserComponent,
    LoginUserComponent,
    UpdateUserComponent,
    JobApplicationComponent,
    JobOfferComponent,
    ListJobOfferComponent,
    ListJobApplierComponent,
    UserProfileComponent,
    DeleteOfferComponent,
    JobOfferDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    MatCard,
    MatCardContent,
    MatCardHeader,
    MatButton,
    MatCardActions,
    MatCardModule,
    MatPaginator
  ],
  providers: [
    provideClientHydration(),
    provideAnimationsAsync()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
