import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AddUserComponent} from "./components/add-user/add-user.component";
import {ListUserComponent} from "./components/list-user/list-user.component";
import {DeleteUserComponent} from "./components/delete-user/delete-user.component";
import {DetailsUserComponent} from "./components/details-user/details-user.component";
import {LoginUserComponent} from "./components/login-user/login-user.component";
import {UpdateUserComponent} from "./components/update-user/update-user.component";
import {JobOfferComponent} from "./components/job-offer/job-offer.component";
import {ListJobOfferComponent} from "./components/list-job-offer/list-job-offer.component";
import {ListJobApplierComponent} from "./components/list-job-applier/list-job-applier.component";
import {DeleteOfferComponent} from "./components/delete-offer/delete-offer.component";
import {JobOfferDetailsComponent} from "./components/job-offer-details/job-offer-details.component";

const routes: Routes = [
  { path: 'add-user', component: AddUserComponent },
  { path: 'update-user', component: UpdateUserComponent },
  { path: 'job-offer', component: JobOfferComponent },
  { path: 'delete-offer', component: DeleteOfferComponent },
  { path: 'job-details/:id', component: JobOfferDetailsComponent },
  { path: 'list-user', component: ListUserComponent },
  { path: 'list-job-offer', component: ListJobOfferComponent },
  { path: 'list-job-applier', component: ListJobApplierComponent },
  { path: 'delete-user', component: DeleteUserComponent },
  { path: 'user-details/:id', component: DetailsUserComponent },
  { path: 'login-user', component: LoginUserComponent },
  // Add other routes as needed
  { path: '', redirectTo: '/login-user', pathMatch: 'full' }, // Default route
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
