import { Component } from '@angular/core';
import {User} from "./user";
import {UserService} from "./services/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  // user: User = new User({ $oid: '' }, '', '', '','','','','','','');
  // // Initialize with empty values
  // users?: User[] | null = null;
  //
  // constructor(private userService: UserService) {}
  //
  //
  // ngOnInit(): void {
  //   this.getUsers();
  // }
  // addUser() {
  //   this.userService.addUser(this.user).subscribe(
  //     (response) => {
  //       console.log(response);
  //       // Handle success, update UI or show a success message
  //     },
  //     (error) => {
  //       console.error(error);
  //       // Handle error, show an error message
  //     }
  //   );
  // }
  //
  // getUser(id: string) {
  //   this.userService.getUser(id).subscribe(
  //     (user: User) => {
  //       console.log(user);
  //       // Handle retrieved user data, update UI, etc.
  //     },
  //     (error) => {
  //       console.error(error);
  //       // Handle error, show an error message
  //     }
  //   );
  // }
  //
  // updateUser(id: string) {
  //   this.userService.updateUser(id, this.user).subscribe(
  //     (response) => {
  //       console.log(response);
  //       // Handle success, update UI or show a success message
  //     },
  //     (error) => {
  //       console.error(error);
  //       // Handle error, show an error message
  //     }
  //   );
  // }
  //
  // deleteUser(id: string) {
  //   this.userService.deleteUser(id).subscribe(
  //     (response) => {
  //       console.log(response);
  //       // Handle success, update UI or show a success message
  //     },
  //     (error) => {
  //       console.error(error);
  //       // Handle error, show an error message
  //     }
  //   );
  // }
  //
  // getUsers() {
  //   this.userService.getUsers().subscribe(
  //     (users: User[]) => {
  //       this.users = users;
  //     },
  //     (error) => {
  //       console.error(error);
  //       // Handle error, show an error message
  //     }
  //   );
  // }
}
