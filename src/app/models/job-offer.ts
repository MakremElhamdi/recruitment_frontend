export class JobOffer {
  constructor(
    public _id: { $oid: string },
    public title: string,
    public description: string,
    public requirements: string,
    public offerType: string,
    public publicationDate: string,
    public expirationDate: string,
    public visible: boolean,
    public valide: boolean,
    public companyName: string
  ) {}
}

