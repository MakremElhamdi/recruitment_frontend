export class Candidate {
  constructor(
    public data: {
      _id: { $oid: string },
      firstName: string,
      lastName: string,
      phoneNumber: string,
      emailAddress: string,
      password: string,
      title: string,
      resume_path: string
    }
  ) {
  }
}
