import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map, Observable} from 'rxjs';
import {UserService} from "./user.service";
import {JobOffer} from "../models/job-offer";

@Injectable({
  providedIn: 'root'
})
export class JobOfferService {
  private baseUrl: string = 'http://localhost:5000';

  constructor(private http: HttpClient,private userService:UserService) {}
  private getHeaders(): HttpHeaders {
    const token = this.userService.getAuthToken();

    if (token) {
      return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      });
    } else {
      // Handle the case where the token is not available (optional)
      return new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
  }

  createJobOffer(jobOfferData: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/job_offers`, jobOfferData, { headers: this.getHeaders() });
  }

  updateJobOffer(offerId: string, jobOfferData: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/job_offers/${offerId}`, jobOfferData, { headers: this.getHeaders() });
  }

  getAllOffers(): Observable<any> {
    return this.http.get(`${this.baseUrl}/job_offers`, { headers: this.getHeaders() });
  }

  getOffersByTitle(title: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/job_offers/${title}`, { headers: this.getHeaders() });
  }

  deleteJobOffer(offerId: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/job_offers/${offerId}`, { headers: this.getHeaders() });
  }

  getJobOfferById(offerId: string):Observable<any> {
    return this.http.get(`${this.baseUrl}/job_offers/${offerId}`, { headers: this.getHeaders() });
  }
}
