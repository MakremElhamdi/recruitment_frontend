import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {UserService} from "./user.service";

@Injectable({
  providedIn: 'root'
})
export class RecruiterService {
  private baseUrl: string = 'http://localhost:5000';

  constructor(private http: HttpClient,private userService:UserService) {}
  private getHeaders(): HttpHeaders {
    const token = this.userService.getAuthToken();
    console.log('aaaaaaaaaaa',token);

    if (token) {
      return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      });
    } else {
      // Handle the case where the token is not available (optional)
      return new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
  }
  getRecruiterById(recruiterId: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/recruiters/${recruiterId}`,{ headers: this.getHeaders() });
  }

  getAllRecruiters(): Observable<any> {
    return this.http.get(`${this.baseUrl}/recruiters`,{ headers: this.getHeaders() });
  }

  deleteRecruiter(recruiterId: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/recruiters/${recruiterId}`,{ headers: this.getHeaders() });
  }

  registerRecruiter(recruiterData: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/recruiters/register`, recruiterData);
  }

  updateRecruiter(recruiterId: string, recruiterData: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/recruiters/${recruiterId}`, recruiterData,{ headers: this.getHeaders() });
  }
}
