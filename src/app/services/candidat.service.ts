import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {UserService} from "./user.service";

@Injectable({
  providedIn: 'root'
})
export class CandidateService {
  private baseUrl: string = 'http://localhost:5000';
  constructor(private http: HttpClient,private userService:UserService) {}
  private getHeaders(): HttpHeaders {
    const token = this.userService.getAuthToken();
    console.log('aaaaaaaaaaa',token);

    if (token) {
      return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      });
    } else {
      // Handle the case where the token is not available (optional)
      return new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
  }

  getCandidateById(candidateId: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/candidates/${candidateId}`,{ headers: this.getHeaders() });
  }

  getAllCandidates(): Observable<any> {
    return this.http.get(`${this.baseUrl}/candidates`,{ headers: this.getHeaders() });
  }

  deleteCandidate(candidateId: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/candidates/${candidateId}`,{ headers: this.getHeaders() });
  }

  registerCandidate(candidateData: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/candidates/register`, candidateData);
  }

  updateCandidate(candidateId: string, candidateData: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/candidates/${candidateId}`, candidateData,{ headers: this.getHeaders() });
  }
}
