import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, tap} from 'rxjs';
import {User} from "../user";
import {Candidate} from "../models/candidate";
import {Recruiter} from "../models/recruiter";
import {response} from "express";


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUrl = 'http://localhost:5000';
  private authTokenKey: string = 'jwtToken';
  private userType: string | null = null;

  constructor(private http: HttpClient) {}

  getUser(id: string): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/user/${id}`);
  }


  getAllCandidates(): Observable<any> {
    return this.http.get(`${this.baseUrl}/candidates`);
  }

  getAllRecruiters(): Observable<any> {
    return this.http.get(`${this.baseUrl}/recruiters`);
  }


  updateCandidate(candidateId: string, updateData: User): Observable<any> {
    return this.http.put(`${this.baseUrl}/update/candidate/${candidateId}`, updateData);
  }

  updateRecruiter(recruiterId: string, updateData: User): Observable<any> {
    return this.http.put(`${this.baseUrl}/update/recruiter/${recruiterId}`, updateData);
  }
  deleteCandidate(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/candidate/${id}`);
  }
  deleteRecruiter(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/recruiter/${id}`);
  }
  getAllUsers(): Observable<any> {
    // Ajoutez le jeton JWT à la requête ici
    const headers = this.getHeaders();
    return this.http.get(`${this.baseUrl}/users`, { headers });
  }

  getAllUsersByType(userType: string): Observable<any> {
    // Ajoutez le jeton JWT à la requête ici
    const headers = this.getHeaders();
    return this.http.get(`${this.baseUrl}/users/${userType}`, { headers });
  }

  registerAdmin(adminData: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/admin/register`, adminData);
  }

  updateAdmin(adminId: string, adminData: any): Observable<any> {
    // Ajoutez le jeton JWT à la requête ici
    const headers = this.getHeaders();
    return this.http.put(`${this.baseUrl}/admin/${adminId}`, adminData, { headers });
  }

  deleteAdmin(adminId: string): Observable<any> {
    // Ajoutez le jeton JWT à la requête ici
    const headers = this.getHeaders();
    return this.http.delete(`${this.baseUrl}/admin/${adminId}`, { headers });
  }
  login(credentials: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(`${this.baseUrl}/login`, credentials, { headers })
      .pipe(tap((response:any)=>{
        if (response && response.user_data.access_token){
          this.setAuthToken(response.user_data.access_token);
          this.userType = response.user_data.type;
          if (typeof this.userType === "string") {
            localStorage.setItem('userType', this.userType);
          }
          console.log("aaaaaaaaa",this.userType);
          console.log("response",response);
        }
      }))

  }
  getUserType(): string | null {
    return this.userType;
  }

  setUserType(userType: string): void {
    this.userType = userType;
  }
  setAuthToken(token: string): void {
    localStorage.setItem(this.authTokenKey, token);
  }

  getAuthToken(): string | null {
    if (typeof localStorage !== 'undefined') {
      return localStorage.getItem(this.authTokenKey);
    } else {
      // Handle the case where localStorage is not available (optional)
      return null;
    }
  }
  private getHeaders(): HttpHeaders {
    const token = this.getAuthToken();
    console.log('aaaaaaaaaaa',token);

    if (token) {
      return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      });
    } else {
      return new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
  }


  logout(): void {
    localStorage.removeItem(this.authTokenKey);
  }
}
