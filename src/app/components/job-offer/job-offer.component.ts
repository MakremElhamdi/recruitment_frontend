import {Component, OnInit} from '@angular/core';
import {JobOffer} from "../../models/job-offer";
import {JobOfferService} from "../../services/job-offer.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-job-offer',
  templateUrl: './job-offer.component.html',
  styleUrl: './job-offer.component.css'
})
export class JobOfferComponent implements OnInit{
  jobOffer: JobOffer = new JobOffer({ $oid: '' }, '', '', '', '', '', '', false, false, '');

  constructor(private jobOfferService: JobOfferService,private router: Router, private route: ActivatedRoute) { }
  ngOnInit() {
    const offerId = this.route.snapshot.paramMap.get('id');
    const isUpdate = this.route.snapshot.paramMap.get('update') === 'true';

    if (isUpdate && offerId) {
      this.jobOfferService.getJobOfferById(offerId).subscribe(
        (offer) => {
          this.jobOffer = offer;
        },
        (error) => {
          console.error('Erreur lors de la récupération des détails de l\'offre:', error);
        }
      );
    }
  }

  onSubmit() {
    if (this.jobOffer._id.$oid) {
      // If it's an update, call the updateJobOffer service
      this.jobOfferService.updateJobOffer(this.jobOffer._id.$oid, this.jobOffer).subscribe(response => {
        console.log('Offre mise à jour:', response);
        this.router.navigate(['/list-job-offer']);
        // Additional logic if needed after update
      });
    } else {
      // If it's a new creation, call the createJobOffer service
      this.jobOfferService.createJobOffer(this.jobOffer).subscribe(response => {
        console.log('Nouvelle offre créée:', response);
        this.router.navigate(['/list-job-offer']);
        // Additional logic if needed after creation
      });
    }
  }


  previousState() {
    window.history.back();
  }
}
