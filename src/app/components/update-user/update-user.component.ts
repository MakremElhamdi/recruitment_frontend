import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../user';
import { UserService } from '../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css'],
})
export class UpdateUserComponent implements OnInit {
  user: User = new User({ $oid: '' }, '', '', '', '', '', '', '', '', '',''); // Initialize with empty values
  isUpdate: boolean = false;
  userType: string = '';
  userForm: FormGroup;

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.userForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      emailAddress: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      address: [''], // Contrôle spécifique à 'recruiter'
      name: [''], // Contrôle spécifique à 'recruiter'
      title: [''], // Contrôle spécifique à 'candidat'
      resume_path: [''], // Contrôle spécifique à 'candidat'
      // Ajoutez d'autres champs si nécessaire
    });

  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      const userString = params['user'];
      this.isUpdate = params['isUpdate'] === 'true';
      this.userType = params['userType'];
      if (userString && this.isUpdate) {
        const user = JSON.parse(userString);
        this.user = user;
        // Update form values when user is available
        this.userForm.patchValue(this.user);
      }
    });
  }

  previousState(): void {
    this.router.navigate(['/list-user']);
  }

  updateUser() {
    // Get form values
    const updatedUser = this.userForm.value;

    if (this.userType === 'candidat') {
      this.userService
        .updateCandidate(this.user._id.$oid, updatedUser)
        .subscribe(
          (response) => {
            console.log(response);
            // Handle success, update UI or show a success message
            this.router.navigate(['/list-user']);
          },
          (error) => {
            console.error(error);
            // Handle error, show an error message
          }
        );
    } else {
      this.userService
        .updateRecruiter(this.user._id.$oid, updatedUser)
        .subscribe(
          (response) => {
            console.log(response);
            // Handle success, update UI or show a success message
            this.router.navigate(['/list-user']);
          },
          (error) => {
            console.error(error);
            // Handle error, show an error message
          }
        );
    }
  }
}
