import {Component, OnInit, ViewChild} from '@angular/core';
import {JobOffer} from "../../models/job-offer";
import {JobOfferService} from "../../services/job-offer.service";
import {Router} from "@angular/router";
import {UserService} from "../../services/user.service";
import {DeleteUserComponent} from "../delete-user/delete-user.component";
import {MatDialog} from "@angular/material/dialog";
import {DeleteOfferComponent} from "../delete-offer/delete-offer.component";
import {User} from "../../user";
import {MatPaginator, PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-list-job-offer',
  templateUrl: './list-job-offer.component.html',
  styleUrl: './list-job-offer.component.css'
})
export class ListJobOfferComponent implements OnInit{
  jobOffers: JobOffer[] = [];
  pageSize = 10;
  currentPage = 0;
  userType!: string | null;
  companyName!: string | null;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private jobOfferService: JobOfferService,private router:Router,private userService:UserService,public dialog: MatDialog,) {}

  ngOnInit() {

    this.userType = localStorage.getItem('userType');
    console.log(this.userType,'eeeeeeeeeeeeeeeeee')
    this.getAllJobOffers();
  }

  getAllJobOffers() {
    const startIndex = this.currentPage * this.pageSize;
    const endIndex = (this.currentPage + 1) * this.pageSize;
    this.jobOfferService.getAllOffers().subscribe(
      (offers) => {
        this.jobOffers = offers;
        this.jobOffers = offers.slice(startIndex, endIndex);
        this.paginator.length = offers.length;
      },
      (error) => {
        console.error('Erreur lors de la récupération des offres:', error);
      }
    );
  }
  onPageChange(event: PageEvent) {
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getAllJobOffers();
  }
  redirectToCreateOffer() {
    this.router.navigate(['/job-offer']);
  }

  updateOffre(offerId: string) {
    this.router.navigate(['/job-offer', { id: offerId, update: true }]);
  }


  viewJobOfferDetails(offer: JobOffer) {
    this.router.navigate(['/job-details', offer._id.$oid]);
  }

  Delete(id:string) {
    this.jobOfferService.deleteJobOffer(id).subscribe(
      (response) => {
        console.log(response);
        // Handle success, update UI or show a success message
        this.getAllJobOffers(); // Refresh the user list after deleting a user
      },
      (error) => {
        console.error(error);
        // Handle error, show an error message
      }
    );
  }
  openDeleteConfirmationDialog(jobId: string): void {
    const dialogRef = this.dialog.open(DeleteOfferComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // User confirmed delete, call delete method
        this.Delete(jobId);
      }
    });
  }


  logout() {
    this.userService.logout();
    this.router.navigate(['/login-user']);
  }
}
