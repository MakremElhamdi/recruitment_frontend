import {Component, OnInit} from '@angular/core';
import {JobOffer} from "../../models/job-offer";
import {ActivatedRoute} from "@angular/router";
import {JobOfferService} from "../../services/job-offer.service";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-job-offer-details',
  templateUrl: './job-offer-details.component.html',
  styleUrl: './job-offer-details.component.css'
})
export class JobOfferDetailsComponent implements OnInit{
  offer: JobOffer | undefined;
  userType!: string | null;

  constructor(
    private route: ActivatedRoute,
    private jobOfferService: JobOfferService,
    private userService:UserService
  ) {}

  ngOnInit(): void {
    this.userType = this.userService.getUserType();
    this.route.params.subscribe(params => {
      const jobId = params['id'];
      if (jobId) {
        this.jobOfferService.getJobOfferById(jobId).subscribe(
          (jobOffer: JobOffer) => {
            this.offer = jobOffer;
          },
          (error) => {
            console.error(error);
            // Handle error, show an error message
          }
        );
      }
    });
  }
  previousState(): void {
    window.history.back();
  }
  postuler() {
    console.log("Postuler à l'offre !");
  }

}
