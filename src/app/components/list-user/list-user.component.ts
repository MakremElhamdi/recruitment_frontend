import {Component, OnInit} from '@angular/core';
import {User} from "../../user";
import {UserService} from "../../services/user.service";
import {DeleteUserComponent} from "../delete-user/delete-user.component";
import {MatDialog} from "@angular/material/dialog";
import {AddUserComponent} from "../add-user/add-user.component";
import {ActivatedRoute, Router, Routes} from "@angular/router";

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrl: './list-user.component.css'
})
export class ListUserComponent implements OnInit{
  users: User[] | null = null;
  userType: string | undefined;
  constructor(private userService: UserService, public dialog: MatDialog,private router:Router,private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.getUsers();
    this.route.queryParams.subscribe(params => {
      this.userType = params['userType'];
      console.log(this.userType);
    });
  }

  redirectToAddUser() {
    this.router.navigate(['/add-user']);
  }
  viewUserDetails(user: User) {
    this.router.navigate(['/user-details', user._id.$oid]);
  }

  getUsers() {
    if (this.userType === 'candidat') {
      this.userService.getAllCandidates().subscribe(
        (users: User[]) => {
          this.users = users;
        },
        (error) => {
          console.error(error);
          // Handle error, show an error message
        }
      );
    }else{
      this.userService.getAllRecruiters().subscribe(
        (users: User[]) => {
          this.users = users;
        },
        (error) => {
          console.error(error);
          // Handle error, show an error message
        }
      );
    }

  }

  openDeleteConfirmationDialog(userId: string): void {
    const dialogRef = this.dialog.open(DeleteUserComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // User confirmed delete, call delete method
        this.deleteUser(userId);
      }
    });
  }

  openUpdateDialog(user: User): void {
    this.router.navigate(['/update-user'], {
      queryParams: {
        user: JSON.stringify(user),
        isUpdate: 'true',
        userType: this.userType
      }
    });
    console.log(this.userType, 'aaaaaaaaaaaaaa');
  }
  logout() {
    this.router.navigate(['/login-user']);
  }


  deleteUser(id: string) {
    if (this.userType === 'candidat') {
      this.userService.deleteCandidate(id).subscribe(
        (response) => {
          console.log(response);
          // Handle success, update UI or show a success message
          this.getUsers(); // Refresh the user list after deleting a user
        },
        (error) => {
          console.error(error);
          // Handle error, show an error message
        }
      );
    }else{
      this.userService.deleteRecruiter(id).subscribe(
        (response) => {
          console.log(response);
          // Handle success, update UI or show a success message
          this.getUsers(); // Refresh the user list after deleting a user
        },
        (error) => {
          console.error(error);
          // Handle error, show an error message
        }
      );
    }

  }

}
