import { Component } from '@angular/core';
import {User} from "../../user";
import {UserService} from "../../services/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css']
})
export class LoginUserComponent {
  user: User = new User({ $oid: '' }, '', '', '','','','','','','','');
  constructor(private userService: UserService,private router:Router,private route: ActivatedRoute,private toastr: ToastrService) {}
  ngOnInit(): void {

  }
  previousState(): void {
   this.router.navigate(['add-user']);
  }
  createAccount(userType: string): void {
    this.router.navigate(['/add-user'], { queryParams: { userType: userType } });
  }
  LoginUser() {
    const credentials = {
      emailAddress: this.user.emailAddress,
      password: this.user.password
    };

    this.userService.login(credentials).subscribe(
      (response) => {
        if (response.login) {
          console.log('Login successful');
          this.toastr.success('Login success');
          this.userService.setUserType(response.user_data.type);

          // Rediriger vers ListJobOfferComponent si le type d'utilisateur est "recruiter"
          if (response.user_data.type === 'admin') {
            this.router.navigateByUrl('/list-user');
          } else
          {
            this.router.navigateByUrl('/list-job-offer');
          }
        } else {
          console.log('Login failed: ', response.message);
          this.toastr.error(response.error.message);
        }
        console.log(response, 'aaaaaaaa');
      },
      (error) => {
        console.error('Error during login:', error);
        this.toastr.error(error.error.message);
      }
    );
  }
}
