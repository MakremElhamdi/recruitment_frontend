import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListJobApplierComponent } from './list-job-applier.component';

describe('ListJobApplierComponent', () => {
  let component: ListJobApplierComponent;
  let fixture: ComponentFixture<ListJobApplierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListJobApplierComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ListJobApplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
