import {Component, OnInit} from '@angular/core';
import {User} from "../../user";
import {UserService} from "../../services/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {response} from "express";
import {CandidateService} from "../../services/candidat.service";
import {RecruiterService} from "../../services/recruiter.service";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrl: './add-user.component.css'
})
export class AddUserComponent implements OnInit{
  user: User = new User({ $oid: '' }, '', '', '','','','','','','','');// Initialize with empty values
  users?: User[] | null = null;
  isUpdate: boolean = false;
  constructor(private userService: UserService, private candidatService:CandidateService,private recruiterService:RecruiterService, private router:Router,private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      const userString = params['user'];
      this.isUpdate = params['isUpdate'] === 'true';
      if (userString && this.isUpdate) {
        const user = JSON.parse(userString);
        this.user = user;
      }
    });
  }
  previousState(): void {
    this.router.navigate(['/login-user']);
  }
  saveUser() {
    if (this.isUpdate) {
      this.updateUser();
    } else {
      this.addUser();
    }
  }
  addUser() {
    if (this.user.type === 'candidat') {
      console.log(this.user.type,'candidat')
      this.candidatService.registerCandidate(this.user).subscribe(
        (response) => {
          console.log(response);
          // Handle success, update UI or show a success message
          this.router.navigate(['/login-user'],{queryParams:{userType:this.user.type}});
        },
        (error) => {
          console.error(error);
          // Handle error, show an error message
        }
      );
    }else if (this.user.type === 'recruiter') {
      console.log(this.user.type,'recruiter')
      this.recruiterService.registerRecruiter(this.user).subscribe(
        (response) => {
          console.log(response);
          this.router.navigate(['/login-user'],{queryParams:{userType:this.user.type}});
        },
        (error) => {
          console.error(error);
        }
      );
    }else {
      console.log(this.user.type,'admin')
      this.userService.registerAdmin(this.user).subscribe(
        (response)=>{
          console.log(response);
          this.router.navigate(['/login-user'],{queryParams:{userType:this.user.type}});
      },
        (error) => {
          console.error(error);
        }
      )
    }
  }
  updateUser() {
    if (this.user.type === 'candidat') {
      this.userService.updateCandidate(this.user._id.$oid, this.user).subscribe(
        (response) => {
          console.log(response);
          // Handle success, update UI or show a success message
          this.router.navigate(['/list-user'], { queryParams: { userType: this.user.type } });

        },
        (error) => {
          console.error(error);
          // Handle error, show an error message
        }
      );
    }else {
      this.userService.updateRecruiter(this.user._id.$oid, this.user).subscribe(
        (response) => {
          console.log(response);
          // Handle success, update UI or show a success message
          this.router.navigate(['/list-user'], { queryParams: { userType: this.user.type } });

        },
        (error) => {
          console.error(error);
          // Handle error, show an error message
        }
      );
    }
    }

}
