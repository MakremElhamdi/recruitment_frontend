import {Component, OnInit} from '@angular/core';
import {User} from "../../user";
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-details-user',
  templateUrl: './details-user.component.html',
  styleUrl: './details-user.component.css'
})
export class DetailsUserComponent implements OnInit{
  user: User | undefined;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const userId = params['id'];
      if (userId) {
        this.userService.getUser(userId).subscribe(
          (user: User) => {
            this.user = user;
          },
          (error) => {
            console.error(error);
            // Handle error, show an error message
          }
        );
      }
    });
  }
  previousState(): void {
    window.history.back();
  }
}
