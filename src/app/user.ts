export class User {
  constructor(
    public _id: { $oid: string },
    public firstName: string,
    public lastName: string,
    public phoneNumber: string,
    public emailAddress: string,
    public password: string,
    public type: string,
    public name: string,
    public address: string,
    public title: string,
    public resume_path: string
  ) {}
}


